<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'ono');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W_62eyf]KQ&3c|_X-&WQ0XJb@0r]h4;MeWyx:t[EEo+F#w:-E+WlIfw@1zNXDx%q');
define('SECURE_AUTH_KEY',  'E,NRTl]Tv#]=$uD0VB7 291]_W_c/fgOXD0<*TGV>N.4vGo5r+X?Z<J{s4~ud:l[');
define('LOGGED_IN_KEY',    '_35RJYiwaRD-Nt=9xEJ&^Zs`Yhx16<_5W}^bk!r7A(_BJNZ^NO7.*m^D/58C91 e');
define('NONCE_KEY',        '[(y0Ly+vl1rnVmO{rSMB=^)TzBx~ON;#2^CDkA|UrV]@QLRcmkXuJu3|oq0[HaF!');
define('AUTH_SALT',        'PSAmsDXJ+_$nHwnTJKA(tcc}|RM)_*=/]hE9#a@Y=ERF)xv,u.O>oVL/y_8|XB<9');
define('SECURE_AUTH_SALT', 'nRO(RWXjRmw$xE`b g6#S3R}z^_)MSHfFafpjh;m35(D2?*pf&3)~HO*Ib6Q!3Z<');
define('LOGGED_IN_SALT',   ' z{m=wV^Hcsai0^x`nX#mFc#[+Js%9yz0oyu^Jf~&&p=SwPBv*9r]}IEPw[Sh4=L');
define('NONCE_SALT',       '|/aHCGG9sB$1M(5ezVFTB$ {f&I:gQLB[tJ}Shh+$cuMN&o,B?Etgdam~ZDFm{79');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
